import request from '../config/common';

export async function deckNoJockers() {

    const res = await request
        .get('deck/new/');
    return res.body;
};

export const deckJockers = (async () =>{
    const res = await request
        .get('deck/new/?jokers_enabled=true');
    return res.body;
});