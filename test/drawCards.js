import request from '../config/common';
import { expect } from "chai";
import { before } from 'mocha';
import {cardsCount} from '../config/common';
const {
    deckNoJockers,
    deckJockers
} = require('../helper/decks')

describe('Draw cards /no jockers', () =>{

    let deck, cardsRemaining

    before(async () =>{
        deck = await deckNoJockers()
    });

    it('Draw single card', async() =>{
        const res = await request
        .get(`deck/${deck.deck_id}/draw/`)
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deck.deck_id);
        expect(Object.keys(res.body.cards)).to.have.lengthOf(1);
        expect(res.body.remaining).to.eq(deck.remaining - (Object.keys(res.body.cards).length));
        expect(res.body.success).to.be.true;
        cardsRemaining = res.body.remaining;
    });

    // depends on the previous test
    it('Draw multiple cards', async() =>{
        const res = await request
        .get(`deck/${deck.deck_id}/draw/?count=${cardsCount}`)
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deck.deck_id);
        expect(Object.keys(res.body.cards)).to.have.lengthOf(cardsCount);
        expect(res.body.remaining).to.eq(cardsRemaining - (Object.keys(res.body.cards).length));
        expect(res.body.success).to.be.true;
        
    });
});


describe('Draw cards /jockers', () =>{
    let deckJ, cardsRemainingJ

    before(async () =>{
        deckJ= await deckJockers()
    });
    
    it('Draw single card', async() =>{
        const res = await request
        .get(`deck/${deckJ.deck_id}/draw/`)
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deckJ.deck_id);
        expect(Object.keys(res.body.cards)).to.have.lengthOf(1);
        expect(res.body.remaining).to.eq(deckJ.remaining - (Object.keys(res.body.cards).length));
        expect(res.body.success).to.be.true;
        cardsRemainingJ = res.body.remaining;
    });

    it('Draw multiple cards', async() =>{
        const res = await request
        .get(`deck/${deckJ.deck_id}/draw/?count=${cardsCount}`)
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deckJ.deck_id);
        expect(Object.keys(res.body.cards)).to.have.lengthOf(cardsCount);
        expect(res.body.remaining).to.eq(cardsRemainingJ - (Object.keys(res.body.cards).length));
        expect(res.body.success).to.be.true;
        cardsRemainingJ = res.body.remaining;
    });
});