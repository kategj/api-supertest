import request from '../config/common';
import { expect } from "chai";
import { before } from 'mocha';
const {
    deckNoJockers,
    deckJockers
} = require('../helper/decks')


describe('Deck with Jockers', () => {

    let deckJ

    before(async () =>{
        deckJ= await deckJockers()
    });

    it(' Deck with jockers', async () =>{
        const res = await request
        .get(`deck/${deckJ.deck_id}/`)
        console.log(res.body);
        expect(res.body.success).to.be.true;
        expect(res.body.deck_id).to.eq(deckJ.deck_id);
        expect(res.body.remaining).to.eq(54);
        expect(res.body.shuffled).to.eq(false);
    });
});

describe('Deck w/o Jockers', () => {

    let deck

    before(async () =>{
        deck= await deckNoJockers()
    });

    it(' Create new deck w/o jockers', async () =>{
        const res = await request
        .get(`deck/${deck.deck_id}/`)
        console.log(res.body);
        expect(res.body.success).to.be.true;
        expect(res.body.deck_id).to.eq(deck.deck_id);
        expect(res.body.remaining).to.eq(52);
        expect(res.body.shuffled).to.eq(false);
    });
});
