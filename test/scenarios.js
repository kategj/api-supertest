import request from '../config/common';
import { expect } from "chai";

describe ('Deck w/o jockers /scenarios', () =>{

    let deck_id, remaining, cardsRemainingFirstDraw, cardsRemainingSecondDraw, 
    cardCodes, drawnCardCode, pileName, pileName2, pileCard

    it(' Create new deck w/o jockers', async () =>{
        const res = await request
        .get('deck/new/');
        console.log(res.body);
        expect(res.body.remaining).to.eq(52);
        expect(res.body.shuffled).to.eq(false);
        deck_id = res.body.deck_id;
        remaining = res.body.remaining; 
    })

    it ('Shuffle the deck', async () =>{
        const res = await request
        .get(`deck/${deck_id}/shuffle/`);
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deck_id);
        expect(res.body.shuffled).to.eq(true);
   })

   it ('Draw a card /player1', async () =>{
       const res = await request
       .get(`deck/${deck_id}/draw/`);
       expect(res.body.deck_id).to.eq(deck_id);
       expect(res.body.cards).to.have.lengthOf(1);
       expect(res.body.remaining).to.eq(remaining-1);
       cardsRemainingFirstDraw = res.body.remaining //cards remaining in the deck after the test
       
       const card = res.body.cards
       drawnCardCode = card.map(function(ca) {
           return ca.code
       })
       console.log(drawnCardCode);
   })

   it ('Draw cards /player2', async () =>{
        const res = await request
        .get(`deck/${deck_id}/draw/?count=5`);
        expect(res.body.deck_id).to.eq(deck_id);
        expect(res.body.cards).to.have.lengthOf(5);
        expect(res.body.remaining).to.eq(cardsRemainingFirstDraw-(Object.keys(res.body.cards).length));
        cardsRemainingSecondDraw = res.body.remaining; //cards remaining in the deck after the test
        
        const cards = res.body.cards
        cardCodes = cards.map(function(c) {
            return c.code
        });
        console.log(cardCodes);
   })

   it ('Reshuffle /remaining cards', async () =>{
       const res = await request
       .get(`deck/${deck_id}/shuffle/?remaining=true`);
       console.log(res.body);
       expect(res.body.deck_id).to.eq(deck_id);
       expect(res.body.remaining).to.eq(cardsRemainingSecondDraw);
   })

   xit ('Reshuffle /full deck', async () =>{
       const res = await request
       .get(`deck/${deck_id}/shuffle/`);
       console.log(res.body);
       expect(res.body.deck_id).to.eq(deck_id);
       expect(res.body.remaining).to.eq(remaining);

   })

   it ('Adding card /player1/ to pile', async () =>{
       pileName = 'Player1'
       const res = await request
       .get(`deck/${deck_id}/pile/${pileName}/add/?cards=${drawnCardCode}`)
       console.log(res.body);
       expect(res.body.deck_id).to.eq(deck_id);
       expect(res.body.remaining).to.eq(cardsRemainingSecondDraw);
       expect((Object.keys(res.body.piles)[0])).to.eq(pileName);
       expect(res.body.piles[pileName].remaining).to.eq(drawnCardCode.length);    
   })

   it ('Adding card /player2/ to pile', async () =>{
        pileName2 = 'Player2'
        const res = await request
        .get(`deck/${deck_id}/pile/${pileName2}/add/?cards=${cardCodes}`)
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deck_id);
        expect(res.body.remaining).to.eq(cardsRemainingSecondDraw);
        expect((Object.keys(res.body.piles)[1])).to.eq(pileName2);
        expect(res.body.piles[pileName2].remaining).to.eq(cardCodes.length); 
   })

   it ('Listing cards in pile /player1', async () =>{
       const res = await request
       .get(`deck/${deck_id}/pile/${pileName}/list/`)
       console.log(res.body)
       const player1Cards = res.body.piles[pileName].cards
       pileCard = player1Cards.map(function(a) {
           return a.code
       });
       expect(res.body.deck_id).to.eq(deck_id);
       expect(res.body.remaining).to.eq(cardsRemainingSecondDraw);
       expect(Object.keys(res.body.piles)).to.have.lengthOf(2);
       expect(Object.keys(res.body.piles)).to.include(pileName, pileName2);
       expect(pileCard).to.deep.eq(drawnCardCode);
   })
})