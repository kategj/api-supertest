import request from '../config/common';
import { expect } from "chai";
import { before } from 'mocha';
const {
    deckNoJockers,
    deckJockers}
= require ('../helper/decks');

describe.only('Shuffle deck w/o Jockers', () =>{
    
    let deck

    before(async () =>{
        deck = await deckNoJockers()
        console.log("before")
    });

    beforeEach(async () =>{
        console.log("beforeEach")
    });

    afterEach(() => {
        console.log("afterEach")
    });

    after(() => {
        console.log("after")
    });



    it ('Shuffle the deck w/o Jockers', async () =>{
        const res = await request
        .get(`deck/${deck.deck_id}/shuffle/`);
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deck.deck_id);
        expect(res.body.shuffled).to.be.true;
        expect(res.body.remaining).to.eq(deck.remaining, 52);
        expect(res.body.success).to.be.true;
   });

   it('test2', () => {
    console.log("test2")
   });
});

describe('Shuffle deck with Jockers', () =>{

    let deckJ

    before(async () =>{
        deckJ= await deckJockers()
    });

    it ('Shuffle the deck Jockers', async () =>{
        const res = await request
        .get(`deck/${deckJ.deck_id}/shuffle/`);
        console.log(res.body);
        expect(res.body.deck_id).to.eq(deckJ.deck_id);
        expect(res.body.shuffled).to.be.true;
        expect(res.body.remaining).to.eq(deckJ.remaining);
        expect(res.body.success).to.be.true;
       
   });
});