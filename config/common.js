import url from './url';
const supertest = require('supertest');
const request = supertest(url.baseUrl);
const cardsCount = `${Math.floor(Math.random() * 51)}`;

export default request;
export {cardsCount};